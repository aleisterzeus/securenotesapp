type Note = {
  id?: string | number[];
  title: string;
  note: string;
  created_at?: string;
  updated_at?: string;
};
