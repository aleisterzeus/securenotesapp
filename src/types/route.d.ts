type RootStackParam = {
  Auth: undefined;
  AddEdit: {
    data?: Note;
  };
  List: undefined;
  Onboarding: undefined;
  Splash: undefined;
};
