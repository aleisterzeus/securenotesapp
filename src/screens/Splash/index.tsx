/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect} from 'react';
import {RouteProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
//components
import Text from '@components/Text';
import View from '@components/View';

import styles from './styles';
import Storage from '@utils/Storage';

type Props = {
  navigation: StackNavigationProp<RootStackParam, 'Splash'>;
  route: RouteProp<RootStackParam, 'Splash'>;
};

const Splash: React.FC<Props> = ({navigation}) => {
  useEffect(() => {
    setTimeout(async () => {
      const isSetupAuthenticationCompleted =
        await Storage.isSetupAuthenticationCompleted();
      if (isSetupAuthenticationCompleted) {
        navigation.replace('Auth');
      } else {
        navigation.replace('Onboarding');
      }
    }, 2000);
  }, []);

  return (
    <View style={styles.container}>
      <Text type="medium" size={48}>
        Secure
        <Text type="bold" size={48}>
          Notes
        </Text>
      </Text>
    </View>
  );
};

export default Splash;
