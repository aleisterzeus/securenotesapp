import {StyleSheet} from 'react-native';
import Colors from '@styles/color';
import {scale} from '@utils';

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: Colors.WHITE,
  },
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: scale(250),
    height: scale(250),
    marginBottom: scale(50),
  },
  textCenter: {
    textAlign: 'center',
  },
  button: {
    marginTop: scale(10),
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: Colors.PRIMARY,
    paddingVertical: scale(10),
    paddingHorizontal: scale(20),
    borderRadius: scale(100),
  },
  mr5: {
    marginRight: scale(5),
  },
});

export default styles;
