import React, {useState} from 'react';
import {Image, TouchableOpacity} from 'react-native';
import {StackNavigationProp} from '@react-navigation/stack';
import {RouteProp} from '@react-navigation/native';
import Swiper from 'react-native-swiper';
import WecomeImage from '@assets/images/welcome.png';
import SecureNote from '@assets/images/secure_note.png';
import ReactNativeBiometrics from 'react-native-biometrics'; // Import as namespace

import Icon from '@components/Icon';
import PromptPassword from '@components/PromptPassword';
import Text from '@components/Text';
import View from '@components/View';

import {showToast} from '@utils';
import Storage from '@utils/Storage';

import styles from './styles';
import Colors from '@styles/color';

type Props = {
  navigation: StackNavigationProp<RootStackParam, 'Onboarding'>;
  route: RouteProp<RootStackParam, 'Onboarding'>;
};

const Onboarding: React.FC<Props> = ({navigation}) => {
  const [passwordDialog, setPasswordDialog] = useState(false);

  const handleSetupAuthentication = async () => {
    const isSensorAvailable = await new ReactNativeBiometrics().isSensorAvailable();
    if (isSensorAvailable) {
      verifyBiometrics();
    } else {
      setPasswordDialog(true);
    }
  };

  const verifyBiometrics = () => {
    new ReactNativeBiometrics()
      .simplePrompt({promptMessage: 'Confirm fingerprint'})
      .then(resultObject => {
        const {success} = resultObject;
        if (success) {
          setPasswordDialog(true);
        }
      })
      .catch(() => {
        console.log('biometrics failed');
        showToast('Biometric failed');
      });
  };

  const handleGeneratePassword = async (value: string) => {
    await Storage.generatePassword(value);
    await Storage.setupAuthenticationCompleted();
    navigation.replace('List');
  };

  return (
    <View style={styles.wrapper}>
      <Swiper
        index={0}
        loop={false}
        autoplay={false}
        activeDotColor={Colors.PRIMARY}>
        <View style={styles.slide} paddingX={18}>
          <Image source={WecomeImage} style={styles.image} />
          <Text type="bold" size={18}>
            Hello!
          </Text>
          <Text style={styles.textCenter}>
            Welcome to the{' '}
            <Text type="medium" size={16}>
              Secure
            </Text>
            <Text size={16} type="bold">
              Notes
            </Text>{' '}
            application, a secure and easy to manage your notes.
          </Text>
        </View>
        <View style={styles.slide} paddingX={18}>
          <Image source={SecureNote} style={styles.image} />
          <Text type="bold" size={18}>
            Get started!
          </Text>
          <Text style={styles.textCenter}>
            Let's setup your biometrics and password to keep your notes is
            secure.
          </Text>
          <TouchableOpacity
            style={styles.button}
            onPress={handleSetupAuthentication}>
            <Text type="medium" color={Colors.WHITE} style={styles.mr5}>
              Setup Authentication
            </Text>
            <Icon name="ic_arrow_right" color={Colors.WHITE} />
          </TouchableOpacity>
        </View>
      </Swiper>
      <PromptPassword
        isVisible={passwordDialog}
        onClose={() => setPasswordDialog(false)}
        onConfirm={handleGeneratePassword}
        title="Setup your password"
      />
    </View>
  );
};

export default Onboarding;
