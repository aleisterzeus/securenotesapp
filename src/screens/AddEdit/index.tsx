/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, useState} from 'react';
import {TextInput} from 'react-native';
import {RouteProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import {showToast} from '@utils';
import Storage from '@utils/Storage';

import Button from '@components/Button';
import View from '@components/View';

import styles from './styles';
import Colors from '@styles/color';

type Props = {
  navigation: StackNavigationProp<RootStackParam, 'AddEdit'>;
  route: RouteProp<RootStackParam, 'AddEdit'>;
};

import CryptoJS from 'crypto-js';

const AddEdit: React.FC<Props> = ({navigation, route}) => {
  const routeParams = route.params;
  const [data, setData] = useState<Note>({
    title: '',
    note: '',
  });

  const isButtonDisabled = data.title === '' || data.note === '';

  useEffect(() => {
    checkData();
  }, []);

  const checkData = async () => {
    if (routeParams?.data) {
      const password = (await Storage.getPassword()) || '';

      setData({
        title: routeParams.data?.title,
        note: CryptoJS.AES.decrypt(routeParams.data?.note, password).toString(CryptoJS.enc.Utf8),
      });
    }
  };

  const handleSubmit = async () => {
    const password = (await Storage.getPassword()) || '';
    const payload = {
      ...data,
      note: CryptoJS.AES.encrypt(data.note, password).toString(),
    };

    if (routeParams?.data) {
      await Storage.editNote(routeParams?.data.id, payload);
    } else {
      await Storage.addNote(payload);
    }
    showToast('Succesfully create/edit note!');
    navigation.goBack();
  };

  return (
    <View style={styles.container} paddingY={10} paddingX={10}>
      <TextInput
        style={styles.titleInputText}
        placeholder="Title here.."
        placeholderTextColor={Colors.GREY}
        value={data.title}
        onChangeText={text => setData({...data, title: text})}
      />
      <TextInput
        style={styles.noteInputText}
        placeholder="your note here.."
        placeholderTextColor={Colors.GREY}
        value={data.note}
        onChangeText={text => setData({...data, note: text})}
        multiline={true}
      />
      <Button
        testID="btn-save"
        text="Save"
        containerStyle={styles.button}
        disabled={isButtonDisabled}
        onPress={handleSubmit}
      />
    </View>
  );
};

export default AddEdit;
