import {StyleSheet} from 'react-native';
import {scale} from '@utils';
import Colors from '@styles/color';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  rowBetween: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  titleInputText: {
    fontFamily: 'Poppins-Bold',
    color: Colors.BLACK,
    fontSize: scale(16),
  },
  noteInputText: {
    fontFamily: 'Poppins-Light',
    color: Colors.BLACK,
    fontSize: scale(12),
  },
  button: {
    position: 'absolute',
    right: scale(20),
    bottom: scale(20),
    width: scale(100),
  },
});

export default styles;
