/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, useState} from 'react';
import {Modal, TextInput, TouchableOpacity} from 'react-native';
import {RouteProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import ReactNativeBiometrics from 'react-native-biometrics';

//components
import Button from '@components/Button';
import Icon from '@components/Icon';
import Text from '@components/Text';
import View from '@components/View';

import {showToast} from '@utils';
import Storage from '@utils/Storage';

import styles from './styles';
import Colors from '@styles/color';

const rnBiometrics = new ReactNativeBiometrics();

type Props = {
  navigation: StackNavigationProp<RootStackParam, 'Auth'>;
  route: RouteProp<RootStackParam, 'Auth'>;
};

const Auth: React.FC<Props> = ({navigation}) => {
  const [showPasswordDialog, setPasswordDialog] = useState(false);
  const [password, setPassword] = useState('');

  useEffect(() => {
    rnBiometrics.isSensorAvailable().then(resultObject => {
      const {available} = resultObject;

      if (available) {
        verifyBiometrics();
      } else {
        setPasswordDialog(true);
      }
    });
  }, []);

  const verifyBiometrics = () => {
    rnBiometrics
      .simplePrompt({promptMessage: 'Confirm fingerprint'})
      .then(resultObject => {
        const {success} = resultObject;

        if (success) {
          navigation.navigate('List');
        } else {
          setPasswordDialog(true);
        }
      })
      .catch(() => {
        console.log('biometrics failed');
      });
  };

  const validatePassword = async () => {
    try {
      const isGranted = await Storage.checkPassword(password);
      if (isGranted) {
        navigation.navigate('List');
      } else {
        showToast('Password incorrect');
      }
    } catch (e) {
      showToast('Error');
    }
  };

  const handleClose = () => {
    setPasswordDialog(false);
    verifyBiometrics();
  };

  return (
    <View style={styles.container}>
      <Button
        text="Sign In"
        containerStyle={styles.button}
        onPress={() => setPasswordDialog(true)}
      />
      <Modal visible={showPasswordDialog}>
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <View style={styles.modalHeader}>
              <Text type="bold">You can sign in with password</Text>
              <TouchableOpacity onPress={handleClose}>
                <Icon name="ic_close" />
              </TouchableOpacity>
            </View>
            <TextInput
              style={styles.textInput}
              placeholder="Input your password here.."
              placeholderTextColor={Colors.GREY}
              secureTextEntry
              value={password}
              onChangeText={text => setPassword(text)}
            />
            <Button
              text="Sign In"
              containerStyle={styles.button}
              onPress={validatePassword}
              disabled={password === ''}
            />
          </View>
        </View>
      </Modal>
    </View>
  );
};

export default Auth;
