import {StyleSheet} from 'react-native';

import Colors from '@styles/color';
import {ShadowNormal} from '@styles/shadow';

import {scale} from '@utils';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.WHITE,
  },
  header: {
    paddingHorizontal: scale(13),
    paddingVertical: scale(10),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.BLACK,
    marginBottom: scale(13),
  },
  flatlistContainer: {
    flexGrow: 1,
  },
  rowBetween: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  itemCard: {
    marginTop: scale(3),
    width: scale(150),
    alignSelf: 'center',
    backgroundColor: Colors.WHITE,
    borderRadius: scale(10),
    elevation: scale(3),
    marginHorizontal: scale(13),
    padding: scale(10),
  },
  section: {
    height: scale(50),
  },
  textUpdatedAt: {
    alignSelf: 'flex-end',
  },
  fab: {
    position: 'absolute',
    width: scale(56),
    height: scale(56),
    alignItems: 'center',
    justifyContent: 'center',
    right: scale(20),
    bottom: scale(20),
    backgroundColor: Colors.PRIMARY,
    borderRadius: scale(30),
    elevation: scale(8),
  },
  emptySection: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: scale(20),
  },
  emptyImage: {
    width: scale(200),
    height: scale(200),
    marginBottom: scale(20),
  },
  textCenter: {
    textAlign: 'center',
  },
});

export default styles;
