import React, {useEffect, useState} from 'react';
import {FlatList, Image, TouchableOpacity} from 'react-native';
import {useIsFocused} from '@react-navigation/native';
import {RouteProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';

import Storage from '@utils/Storage';

import Icon from '@components/Icon';
import Text from '@components/Text';
import View from '@components/View';

import styles from './styles';
import Colors from '@styles/color';

type Props = {
  navigation: StackNavigationProp<RootStackParam, 'List'>;
  route: RouteProp<RootStackParam, 'List'>;
};

const List: React.FC<Props> = ({navigation}) => {
  const isFocused = useIsFocused();
  const [data, setData] = useState<Note[]>([]);

  useEffect(() => {
    getListData();
  }, [isFocused]);

  const getListData = async () => {
    const res = await Storage.listNote();
    setData(res);
  };

  const renderItem = ({item, index}: {item: Note; index: number}) => {
    return (
      <TouchableOpacity
        testID={`note-item-card-${index}`}
        style={styles.itemCard}
        key={`note-item-${index}`}
        onPress={() => navigation.navigate('AddEdit', {data: item})}>
        <View style={styles.rowBetween}>
          <Text type="bold">{item.title}</Text>
        </View>
        <View style={styles.section} />
        <Text type="medium" style={styles.textUpdatedAt} size={8}>
          {item.updated_at}
        </Text>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text color={Colors.WHITE} size={16}>
          Secure
          <Text type="bold" size={16} color={Colors.WHITE}>
            Notes
          </Text>
        </Text>
      </View>
      <FlatList
        testID='flatlist'
        data={data}
        numColumns={2}
        contentContainerStyle={styles.flatlistContainer}
        renderItem={renderItem}
        ItemSeparatorComponent={() => <View marginTop={10} />}
        ListEmptyComponent={
          <View style={styles.emptySection}>
            <Image
              source={require('@assets/images/empty.png')}
              style={styles.emptyImage}
            />
            <Text type="bold" size={16}>
              Whooops!
            </Text>
            <Text style={styles.textCenter}>
              You dont have notes yet, let's start writing your note, calm your
              notes will be secure
            </Text>
          </View>
        }
      />
      <TouchableOpacity
        style={styles.fab}
        testID='fab'
        onPress={() => navigation.navigate('AddEdit')}>
        <Icon name="ic_add_note" color={Colors.WHITE} />
      </TouchableOpacity>
    </View>
  );
};

export default List;
