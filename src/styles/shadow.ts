export const ShadowNormal = {
  shadowColor: 'rgba(46, 50, 132, 0.15)',
  shadowOffset: {
    width: 0,
    height: 3,
  },
  shadowOpacity: 0.29,
  shadowRadius: 4.65,
  elevation: 4,
};

export const ShadowTop = {
  shadowColor: 'rgba(46, 50, 132, 0.15)',
  shadowOffset: {
    width: 0,
    height: 12,
  },
  shadowOpacity: 0.58,
  shadowRadius: 16.0,
  elevation: 24,
};

export const ShadowBottom = {
  shadowColor: 'rgba(46, 50, 132, 0.15)',
  shadowOffset: {
    width: 0,
    height: 12,
  },
  shadowOpacity: 0.58,
  shadowRadius: 16.0,
  elevation: 10,
};
