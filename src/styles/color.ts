const Colors = {
  // PRIMARY: '#03A9F4',
  PRIMARY: '#000000',
  BLACK: '#000000',
  WHITE: '#FFFFFF',
  GREY: '#a9a9a9',
};

export default Colors;
