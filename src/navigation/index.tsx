import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
//pages
import AddEdit from '@screens/AddEdit';
import Auth from '@screens/Auth';
import List from '@screens/List';
import Splash from '@screens/Splash';
import Onboarding from '@screens/Onboarding';

const Stack = createStackNavigator<RootStackParam>();

const Route = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Splash">
        <Stack.Screen
          name="AddEdit"
          component={AddEdit}
          options={{title: 'Your Note'}}
        />
        <Stack.Screen
          name="Auth"
          component={Auth}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="List"
          component={List}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Onboarding"
          component={Onboarding}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Splash"
          component={Splash}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Route;
