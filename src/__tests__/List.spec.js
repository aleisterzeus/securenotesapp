import React from 'react';
import List from '@screens/List';
import {render, fireEvent} from '@testing-library/react-native';
import Storage from '@utils/Storage';

let mockStorage = [];
jest.mock('@utils/Storage', () => ({
  listNote: () => {
    return mockStorage;
  },
}));

describe('Test List Screen', () => {
  test('renders list component correctly', async () => {
    const {getByText, getByTestId} = render(<List />);

    const res = await Storage.listNote();
    expect(res.length).toBe(0);

    const flatList = getByTestId('flatlist');
    expect(flatList).toBeTruthy();

    // Testing whether the 'SecureNotes' text is rendered
    const secureText = getByText('SecureNotes');
    expect(secureText).toBeTruthy();

    // Test if the ListEmptyComponent is rendered with the correct text
    const whoopsText = getByText('Whooops!');
    const emptyListTextDescription = getByText(
      "You dont have notes yet, let's start writing your note, calm your notes will be secure",
    );
    expect(whoopsText).toBeTruthy();
    expect(emptyListTextDescription).toBeTruthy();
  });

  test('should flatlist renderItem', async () => {
    mockStorage = [
      {title: 'Note 1', note: 'desc note 1'},
      {title: 'Note 2', note: 'desc note 2'},
    ];
    const navigation = {navigate: jest.fn()};
    const {getByTestId} = render(<List navigation={navigation} />);
    const res = await Storage.listNote();
    expect(res.length).toBe(2);

    const item0 = getByTestId('note-item-card-0');
    fireEvent.press(item0);
    expect(navigation.navigate).toBeCalledWith('AddEdit', {
      data: {...mockStorage[0]},
    });
  });

  test('navigates to AddEdit screen on pressing FAB', () => {
    const navigation = {navigate: jest.fn()};
    const {getByTestId} = render(<List navigation={navigation} />);
    const fabButton = getByTestId('fab');
    fireEvent.press(fabButton);
    expect(navigation.navigate).toBeCalledWith('AddEdit');
  });
});
