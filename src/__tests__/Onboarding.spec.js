import React from 'react';
import {render, fireEvent, waitFor, act} from '@testing-library/react-native';
import Onboarding from '@screens/Onboarding';

// Mocking necessary dependencies
jest.mock('react-native-swiper', () => 'Swiper');
jest.mock('@components/Icon', () => 'Icon');
jest.mock('@components/PromptPassword', () => 'PromptPassword');
jest.mock('@components/Text', () => 'Text');
jest.mock('@components/View', () => 'View');
jest.mock('@utils', () => ({
  showToast: jest.fn(),
  scale: jest.fn(),
}));
jest.mock('@utils/Storage', () => ({
  generatePassword: jest.fn(),
  setupAuthenticationCompleted: jest.fn(),
}));
jest.mock('react-native-biometrics', () => ({
  isSensorAvailable: jest.fn().mockResolvedValue(true),
  simplePrompt: jest.fn().mockResolvedValue({success: true}),
}));

describe('Test Onboarding Screen', () => {
  test('renders slides correctly', () => {
    const {getByText} = render(<Onboarding />);
    const welcomeText = getByText('Hello!');
    const getStartedText = getByText('Get started!');
    expect(welcomeText).toBeTruthy();
    expect(getStartedText).toBeTruthy();
  });

  test('handles setup authentication correctly', async () => {
    const {getByText} = render(
      <Onboarding navigation={{replace: jest.fn()}} />,
    );
    const setupButton = getByText('Setup Authentication');
    await act(async () => {
      fireEvent.press(setupButton);
    });
  });

  // test('handles password generation and navigation correctly', async () => {
  //   const mockNavigation = { replace: jest.fn() };
  //   const { getByText, getByPlaceholderText } = render(<Onboarding navigation={mockNavigation} route={{}} />);
  //   const setupButton = getByText('Setup Authentication');
  //   fireEvent.press(setupButton);
  //   const passwordInput = getByPlaceholderText('Enter Password');
  //   fireEvent.changeText(passwordInput, 'testPassword');
  //   const confirmButton = getByText('Confirm');
  //   fireEvent.press(confirmButton);
  //   await waitFor(() => {
  //     expect(require('@utils/Storage').generatePassword).toHaveBeenCalledWith('testPassword');
  //     expect(require('@utils/Storage').setupAuthenticationCompleted).toHaveBeenCalled();
  //     expect(mockNavigation.replace).toHaveBeenCalledWith('List');
  //   });
  // });
});
