import React from 'react';
import {render, fireEvent, act} from '@testing-library/react-native';
import AddEdit from '@screens/AddEdit';

// Mocking necessary dependencies
jest.mock('@utils/Storage', () => ({
  getPassword: jest.fn().mockResolvedValue('password'),
  addNote: jest.fn().mockResolvedValue(),
  editNote: jest.fn().mockResolvedValue(),
}));
jest.mock('@components/Button', () => 'mocked-button');
jest.mock('@components/View', () => 'mocked-view');

const note = {
  title: 'Note 1',
  note: 'full text description note',
};

describe('Test AddEdit Screen', () => {
  test('renders input fields correctly', () => {
    const {getByPlaceholderText} = render(
      <AddEdit navigation={{}} route={{}} />,
    );
    const titleInput = getByPlaceholderText('Title here..');
    const noteInput = getByPlaceholderText('your note here..');
    expect(titleInput).toBeTruthy();
    expect(noteInput).toBeTruthy();

    //handle change input
    fireEvent.changeText(titleInput, 'Note 1');
    expect(titleInput.props.value).toBe('Note 1');

    fireEvent.changeText(noteInput, 'full text description note');
    expect(noteInput.props.value).toBe('full text description note');
  });

  test('handles submit correctly on Save button press', async () => {
    const mockNavigation = {goBack: jest.fn()};
    const {getByTestId} = render(
      <AddEdit navigation={mockNavigation} route={{data: note}} />,
    );
    await act(async () => {
      const saveButton = getByTestId('btn-save'); // Make sure the testID is accurate
      fireEvent.press(saveButton);
    });
    expect(mockNavigation.goBack).toHaveBeenCalled();
  });

  test('handles submit correctly on Save/Edit button press', async () => {
    const mockNavigation = {goBack: jest.fn()};
    const {getByTestId} = render(
      <AddEdit navigation={mockNavigation} route={{}} />,
    );
    await act(async () => {
      const saveButton = getByTestId('btn-save'); // Make sure the testID is accurate
      fireEvent.press(saveButton);
    });
    expect(mockNavigation.goBack).toHaveBeenCalled();
  });
  // Add more tests as needed for other functionalities
});
