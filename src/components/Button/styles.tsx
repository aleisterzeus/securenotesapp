import {StyleSheet} from 'react-native';
import {scale} from '@utils';
import Colors from '@styles/color';

const styles = StyleSheet.create({
  container: {
    paddingVertical: scale(10),
    paddingHorizontal: scale(13),
    borderRadius: scale(100),
    alignItems: 'center',
    backgroundColor: Colors.PRIMARY,
  },
});

export default styles;
