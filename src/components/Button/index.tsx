import React from 'react';
import {ColorValue, TouchableOpacity, ViewStyle} from 'react-native';

import Text from '@components/Text';

import styles from './styles';
import Colors from '@styles/color';

type Props = {
  testID?: string;
  text: string;
  backgroundColor?: ColorValue;
  containerStyle?: ViewStyle;
  onPress: () => void;
  disabled?: boolean;
};

const Button: React.FC<Props> = props => {
  const {testID, text, backgroundColor, containerStyle, onPress, disabled} = props;
  let customStyle: ViewStyle = {};

  if (backgroundColor) {
    customStyle.backgroundColor = backgroundColor;
  }

  if (disabled) {
    customStyle.opacity = 0.5;
  }

  return (
    <TouchableOpacity
      testID={testID}
      onPress={onPress}
      disabled={disabled}
      style={[styles.container, customStyle, containerStyle]}>
      <Text color={Colors.WHITE} type="medium">
        {text}
      </Text>
    </TouchableOpacity>
  );
};

Button.defaultProps = {
  backgroundColor: Colors.PRIMARY,
  text: 'Button',
};

export default React.memo<Props>(Button);
