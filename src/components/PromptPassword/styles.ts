import {StyleSheet} from 'react-native';
import {colorOpacity, scale} from '@utils';
import Colors from '@styles/color';

const styles = StyleSheet.create({
  modalContainer: {
    backgroundColor: colorOpacity(Colors.BLACK, 5),
    justifyContent: 'center',
    alignItems: 'center',
    padding: scale(20),
    width: '100%',
    height: '100%',
  },
  modalContent: {
    backgroundColor: Colors.WHITE,
    padding: scale(20),
    width: '100%',
    borderRadius: scale(10),
  },
  modalHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: scale(20),
  },
  textInput: {
    borderWidth: scale(1),
    borderColor: Colors.GREY,
    borderRadius: scale(10),
    padding: scale(8),
    width: '100%',
    fontFamily: 'Poppins-Light',
    fontSize: scale(12),
    color: Colors.BLACK,
  },
  button: {
    width: scale(100),
    marginTop: scale(13),
    alignSelf: 'center',
  },
});

export default styles;
