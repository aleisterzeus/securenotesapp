import React, {useState} from 'react';
import {Modal, TextInput, TouchableOpacity} from 'react-native';

import Button from '@components/Button';
import Icon from '@components/Icon';
import Text from '@components/Text';
import View from '@components/View';

import styles from './styles';
import Colors from '@styles/color';

type Props = {
  title: string;
  isVisible: boolean;
  onConfirm: (value: string) => void;
  onClose: () => void;
};

const PromptPassword: React.FC<Props> = props => {
  const {title, isVisible, onConfirm, onClose} = props;
  const [password, setPassword] = useState('');

  const handleConfirm = () => {
    onConfirm(password);
    onClose();
  };

  return (
    <Modal visible={isVisible} transparent>
      <View style={styles.modalContainer}>
        <View style={styles.modalContent}>
          <View style={styles.modalHeader}>
            <Text type="bold">{title}</Text>
            <TouchableOpacity onPress={onClose}>
              <Icon name="ic_close" />
            </TouchableOpacity>
          </View>
          <TextInput
            style={styles.textInput}
            placeholder="Input your password here.."
            placeholderTextColor={Colors.GREY}
            secureTextEntry
            value={password}
            onChangeText={text => setPassword(text)}
          />
          <Button
            text="Sign In"
            containerStyle={styles.button}
            onPress={handleConfirm}
            disabled={password === ''}
          />
        </View>
      </View>
    </Modal>
  );
};

export default React.memo<Props>(PromptPassword);
