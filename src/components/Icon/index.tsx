import React from 'react';
import {ColorValue, Image, ImageStyle} from 'react-native';

const getSource = (name: IconName) => {
  switch (name) {
    case 'ic_add_note':
      return require('../../../assets/icons/ic_add_note.png');
    case 'ic_arrow_right':
      return require('../../../assets/icons/ic_arrow_right.png');
    case 'ic_close':
      return require('../../../assets/icons/ic_close.png');
  }
};

type Props = {
  name: IconName;
  height?: number;
  width?: number;
  size?: number;
  color?: ColorValue;
};

const Icon: React.FC<Props> = props => {
  const {name, height, width, size, color} = props;
  let customStyle: ImageStyle = {};
  if (width) {
    customStyle.width = width;
  }
  if (height) {
    customStyle.height = height;
  }
  if (size) {
    customStyle.width = size;
    customStyle.height = size;
  }
  if (color) {
    customStyle.tintColor = color;
  }

  return <Image source={getSource(name)} style={customStyle} />;
};

Icon.defaultProps = {
  width: 24,
  height: 24,
};

export default React.memo<Props>(Icon);
