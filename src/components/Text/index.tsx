import React from 'react';
import {Text as TextNative, TextStyle} from 'react-native';
import Colors from '@styles/color';
import {scale} from '@utils';

import styles from './styles';

const getFontType = (type?: FontType) => {
  switch (type) {
    case 'bold':
      return styles.bold;
    case 'medium':
      return styles.medium;
    default:
      return styles.regular;
  }
};

type TextDecorationProps =
  | 'none'
  | 'underline'
  | 'line-through'
  | 'underline line-through';

type TextProps = {
  testID?: string;
  children: any;
  style?: TextStyle;
  color?: string;
  type?: FontType;
  size?: number;
  decoration?: TextDecorationProps;
  numberOfLines?: number;
  lineHeight?: number;
};

type Props = TextNative['props'] & TextProps;

const Text: React.FC<Props> = props => {
  const {
    testID,
    children,
    style,
    color,
    type,
    size,
    decoration,
    numberOfLines,
    lineHeight,
    ...rest
  } = props;
  const font = getFontType(type);

  return (
    <TextNative
      testID={testID}
      accessibilityLabel={testID}
      numberOfLines={numberOfLines}
      {...rest}
      style={[
        font,
        {
          color,
          fontSize: size && scale(size),
          textDecorationLine: decoration,
          lineHeight: lineHeight && scale(lineHeight),
        },
        style,
      ]}>
      {children}
    </TextNative>
  );
};

Text.defaultProps = {
  type: 'regular',
  color: Colors.BLACK,
  children: '',
  size: 12,
  testID: 'text',
  decoration: 'none',
};

export default React.memo<Props>(Text);
