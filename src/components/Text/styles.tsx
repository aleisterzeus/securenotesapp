import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  regular: {
    fontFamily: 'Poppins-Light',
  },
  medium: {
    fontFamily: 'Poppins-Medium',
  },
  bold: {
    fontFamily: 'Poppins-Bold',
  },
});

export default styles;
