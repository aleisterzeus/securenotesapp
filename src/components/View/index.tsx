import React from 'react';
import {ColorValue, View as ViewNative, ViewStyle} from 'react-native';
import {scale} from '@utils';
const getCustomStyle = (props: ViewProps) => {
  const {
    padding,
    paddingY,
    paddingX,
    paddingTop,
    paddingBottom,
    paddingLeft,
    paddingRight,
    margin,
    marginY,
    marginX,
    marginTop,
    marginBottom,
    marginLeft,
    marginRight,
    backgroundColor,
  } = props;
  let customStyle: ViewStyle = {};
  if (padding) {
    customStyle.padding = scale(padding);
  }
  if (paddingY) {
    customStyle.paddingVertical = scale(paddingY);
  }
  if (paddingX) {
    customStyle.marginHorizontal = scale(paddingX);
  }
  if (paddingTop) {
    customStyle.paddingTop = scale(paddingTop);
  }
  if (paddingBottom) {
    customStyle.paddingBottom = scale(paddingBottom);
  }
  if (paddingLeft) {
    customStyle.paddingLeft = scale(paddingLeft);
  }
  if (paddingRight) {
    customStyle.paddingRight = scale(paddingRight);
  }
  if (margin) {
    customStyle.margin = scale(margin);
  }
  if (marginY) {
    customStyle.marginVertical = scale(marginY);
  }
  if (marginX) {
    customStyle.marginHorizontal = scale(marginX);
  }
  if (marginTop) {
    customStyle.marginTop = scale(marginTop);
  }
  if (marginBottom) {
    customStyle.marginBottom = scale(marginBottom);
  }
  if (marginLeft) {
    customStyle.marginLeft = scale(marginLeft);
  }
  if (marginRight) {
    customStyle.marginRight = scale(marginRight);
  }
  if (backgroundColor) {
    customStyle.backgroundColor = backgroundColor;
  }
  return customStyle;
};

type ViewProps = {
  style?: ViewStyle;
  padding?: number;
  paddingY?: number;
  paddingX?: number;
  paddingTop?: number;
  paddingBottom?: number;
  paddingLeft?: number;
  paddingRight?: number;
  margin?: number;
  marginY?: number;
  marginX?: number;
  marginTop?: number;
  marginBottom?: number;
  marginLeft?: number;
  marginRight?: number;
  backgroundColor?: ColorValue;
};

type Props = ViewNative['props'] & ViewProps;

const View: React.FC<Props> = props => {
  const {children, style} = props;
  const customStyle = getCustomStyle(props);

  return <ViewNative style={[customStyle, style]}>{children}</ViewNative>;
};

export default React.memo<Props>(View);
