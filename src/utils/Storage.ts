import AsyncStorage from '@react-native-async-storage/async-storage';
import dayjs from 'dayjs';
import uuid from 'react-native-uuid';

const Storage = {
  addNote: async (payload: Note) => {
    try {
      const existData = await AsyncStorage.getItem('notes');
      const arrData = existData ? JSON.parse(existData) : [];

      payload.id = uuid.v4();
      payload.created_at = dayjs().format('MMM DD, YYYY HH:mm');
      payload.updated_at = dayjs().format('MMM DD, YYYY HH:mm');

      await AsyncStorage.setItem(
        'notes',
        JSON.stringify([payload, ...arrData]),
      );
    } catch (error) {
      throw new Error('Error while creating note');
    }
  },
  editNote: async (id: string, payload: Note) => {
    try {
      const existData = await AsyncStorage.getItem('notes');
      let arrData = existData ? JSON.parse(existData) : [];
      payload.updated_at = dayjs().format('MMM DD, YYYY HH:mm');

      arrData = arrData.map((item: Note) => {
        if (item.id === id) {
          return {
            ...item,
            ...payload,
          };
        }
        return item;
      });
      await AsyncStorage.setItem('notes', JSON.stringify([...arrData]));
    } catch (error) {
      throw new Error('Error while update note');
    }
  },
  listNote: async () => {
    const existData = await AsyncStorage.getItem('notes');
    const arrData = existData ? JSON.parse(existData) : [];
    return arrData;
  },
  deleteAll: async () => {
    AsyncStorage.clear();
  },
  isSetupAuthenticationCompleted: async () => {
    const output = await AsyncStorage.getItem('isSetupAuthenticationCompleted');
    return output;
  },
  setupAuthenticationCompleted: async () => {
    await AsyncStorage.setItem('isSetupAuthenticationCompleted', '1');
  },
  generatePassword: async (payload: string) => {
    try {
      AsyncStorage.setItem('password', payload);
    } catch (error) {
      throw new Error('Error while generate password');
    }
  },
  checkPassword: async (payload: string) => {
    try {
      const password = await AsyncStorage.getItem('password');
      if (password === payload) {
        return true;
      } else {
        return false;
      }
    } catch (error) {
      throw new Error('Error while check password');
    }
  },
  getPassword: async () => {
    const output = await AsyncStorage.getItem('password');
    return output;
  },
};
export default Storage;
