import {PixelRatio, Dimensions, ToastAndroid, Platform} from 'react-native';

export const DESIGN_WIDTH = 360;

export const deviceWidth = () => Dimensions.get('window').width;

export const deviceHeight = () => Dimensions.get('window').height;

export const widthPercentage = (value: number) => (deviceWidth() * value) / 100;

export const heightPercentage = (value: number) =>
  (deviceHeight() * value) / 100;

export const scale = (size: number): number => {
  return Math.round(
    PixelRatio.roundToNearestPixel((size * deviceWidth()) / DESIGN_WIDTH),
  );
};

export const showToast = (message: string) => {
  if (Platform.OS === 'android') {
    ToastAndroid.showWithGravityAndOffset(
      message,
      ToastAndroid.SHORT,
      ToastAndroid.BOTTOM,
      0,
      80,
    );
  }
};

export const colorOpacity = (color: string, value: number) => {
  const opacity = Math.floor(0.1 * value * 255).toString(16);
  return color + opacity;
};
